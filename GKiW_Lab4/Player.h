#pragma once

#include "Utils.h";
#include "Object.h"

class Player : public Object
{
public:
	Player();
	~Player();
	void render();
	void update();
	// move functions
	void rotateRight();
	void rotateLeft();
	void moveForward();
	void moveBackward();

public:
	float speed;
	float rotation;
	double rotate;
	float velocity;
	GLuint objectDisplayList;
	void drawDetectionBox();
	bool is_blocked;
	vec3 aproxPosition();
	vec3 abovePosition();
};