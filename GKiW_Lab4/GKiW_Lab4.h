#pragma once
#include "Utils.h"
#include "Player.h"
#include "Bullet.h"
#include "Block.h"
#include "Game.h"


#define PI 3.14159265

// Deklaracje funkcji u�ywanych jako obs�uga zdarze� GLUT-a.
void OnRender();
void OnReshape(int, int);
void OnKeyPress(unsigned char, int, int);
void OnKeyDown(unsigned char, int, int);
void OnKeyUp(unsigned char, int, int);
void OnMouseMove(int, int);
void OnTimer(int);
void renderBitmapString(float x, float y, char* text);
void keyboardInput();
