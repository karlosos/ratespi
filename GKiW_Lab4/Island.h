#pragma once
#include "Object.h"
class Island :
	public Object
{
public:
	Island(vec3 pos);
	~Island();
	virtual void render();
	virtual void update();
	GLuint objectDisplayList;
};

