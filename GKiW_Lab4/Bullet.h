#pragma once
#include "Object.h"
class Bullet : public Object
{
public:
	Bullet(vec3 pos, vec3 dir, bool direction);
	Bullet(vec3 pos, vec3 dir);
	~Bullet();
	void render();
	void update();

private:
	float speed;
	float velocity;
	double distance;
	void drawDetectionBox();
};