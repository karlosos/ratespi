#pragma once
#include "Object.h"
class Block :
	public Object
{
public:
	Block();
	Block(vec3 pos);
	~Block();
	void render();
	void update();
private:
	double rotation;

};

