#pragma once
#include "Game.h"
class GUI
{
public:
	GUI();
	~GUI();
	void render();
	
	unsigned int WIN_WIDTH;
	unsigned int WIN_HEIGHT;
	int points;
	unsigned int boxes;
	int hp;
	unsigned int maxhp;
	void renderStartScreen();
	void renderEndScreen();

private:
	void renderBoxes();
	void renderPoints();
	void renderHP();
	void renderText(std::string s, int offsetFromTop, int offsetLeft = 90);

};

