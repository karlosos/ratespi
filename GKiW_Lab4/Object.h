#pragma once
class Object
{
public:
	Object();
	virtual ~Object();
	virtual void render() = 0;
	virtual void update() = 0;
	bool alive;
	vec3 getPos();
	vec3 getDir();
	double sphere_radius;

protected:
	vec3 pos;
	vec3 dir;
	void drawDetectionSphere();
};

