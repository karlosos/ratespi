#include "stdafx.h"
#include "Enemy.h"


Enemy::Enemy(vec3 pos)
{
	this->objectDisplayList = LoadObj("paper_ship.obj");
	this->pos = pos;
	dir.x = 0.0f;
	dir.y = 0.0f;
	dir.z = -1.0f;
	speed = .38f;
	rotation = 0;
	velocity = 0.38;

	// do wykrywania kolizji sphere
	sphere_radius = 1.5;
	is_blocked = 0;
}


Enemy::~Enemy()
{
}

void Enemy::render()
{
	// model statku
	float m0_amb[] = { 0.8f, 0.2f, 0.8f, 0.7f };
	float m0_dif[] = { 0.8f, 0.2f, 0.8f, 0.9f };
	float m0_spe[] = { 0.1f, 0.1f, 0.1f, 0.5f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m0_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m0_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m0_spe);
	glPushMatrix();
	glTranslatef(pos.x, 0, pos.z);
	glRotatef(rotation + 180.f, 0.0f, 1.0f, 0.0f);
	double rotate = sin(glutGet(GLUT_ELAPSED_TIME) / 500.f) * 30;
	glRotatef(rotate, 0.0f, 0.0f, 1.0f);
	glScalef(1.f, 1.f, 1.f);

	glCallList(objectDisplayList);
	glPopMatrix();
}

void Enemy::update()
{
	// Ruch przod/tyl:
	pos.x += dir.x * speed * .1f;
	pos.y = 0.f;
	pos.z += dir.z * speed * .1f;
}

void Enemy::moveForward()
{
	velocity = speed;
}

void Enemy::moveBackward()
{
	velocity = -2 * speed;
}

void Enemy::randomDirection() {
	// jezeli odleglosc od gracza >= 5 losuj kierunek
	int random_step = std::rand() % 20 - 10;
	if (random_step < 0) {
		rotateRight(random_step);
	}
	else {
		rotateLeft(random_step);
	}
}

void Enemy::rotateRight(int step) {
	rotation += 1;
	float beta = atan2(dir.z, dir.x);
	float inclination = acos(dir.y);
	dir.x = sin(inclination)*cos(+1.f * PI / 180.f + beta);
	dir.z = sin(inclination)*sin(+1.f * PI / 180.f + beta);
	dir.y = cos(inclination);
}

void Enemy::rotateLeft(int step) {
	rotation += 1;
	float beta = atan2(dir.z, dir.x);
	float inclination = acos(dir.y);
	dir.x = sin(inclination)*cos(-1.f * PI / 180.f + beta);
	dir.z = sin(inclination)*sin(-1.f * PI / 180.f + beta);
	dir.y = cos(inclination);
}