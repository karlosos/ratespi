﻿#include "stdafx.h"
#pragma region Zmienne globalne

Game* game;

double T = 0.0;

int mouseX = 0; // aktualna pozycja kursora myszy (x)
int mouseY = 0; // aktualna pozycja kursora myszy (y)

bool captureMouse = false; // czy przechwytywaæ kursor myszy?
bool free3DMovement = false; // czy pozwoliæ na ruch w 3D?

float mouseSensitivity = .15f; // czu³oœæ na ruchy kursora myszy


#pragma endregion

int main(int argc, char* argv[])
{
	//srand(time(NULL));
	glutInit(&argc, argv);

	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1280, 720);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	
	glutCreateWindow("RatesPi");
	glutFullScreen();
	glutDisplayFunc(OnRender);
	glutReshapeFunc(OnReshape);
	glutKeyboardFunc(OnKeyPress);
	glutKeyboardUpFunc(OnKeyUp);
	glutPassiveMotionFunc(OnMouseMove);
	glutMotionFunc(OnMouseMove);
	glutTimerFunc(17, OnTimer, 0);

	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE); // W³¹czenie cullingu - rysowania tylko jednej strony wielok¹tów
	//glCullFace(GL_BACK); // Okreœlenie, któr¹ stronê wielok¹tów chcemy ukrywaæ
	glFrontFace(GL_CCW); // Okreœlenie, jaki kierunek definicji wierzcho³ków oznacza przód wielok¹tu (GL_CCW - przeciwnie do ruchu wskazówek zegara, GL_CW - zgodnie)

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	
	float gl_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);

	// Ustawienie obs³ugi myszy
	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2); // Przesuniêcie kursora na œrodek ekranu
	mouseX = glutGet(GLUT_WINDOW_WIDTH) / 2;
	mouseY = glutGet(GLUT_WINDOW_HEIGHT) / 2;
	glutSetCursor(GLUT_CURSOR_NONE); // Ukrycie kursora

	PlaySound("CascadesLoop.wav", NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);

	// inicjalizacja obiektow
	// gry
	game = Game::getInstance();

	glutMainLoop();

	delete(game);

	return 0;
}

#pragma region Obsluga wejscia

bool keystate[255];

void OnKeyPress(unsigned char key, int x, int y) {
	if (!keystate[key]) {
		keystate[key] = true;
		OnKeyDown(key, x, y);
	}
}

void OnKeyDown(unsigned char key, int x, int y) {
	if (key == 27) {
		glutLeaveMainLoop();
	}
	if (key == 'm' || key == 'M') { // W³¹czenie/wy³¹czenie przechwytywania kursora myszy - "uwalnia" mysz
		if (captureMouse) {
			captureMouse = false;
			glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
		}
		else {
			captureMouse = true;
			glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
			glutSetCursor(GLUT_CURSOR_NONE);
		}
	}
	if (key == 'p' || key == 'P') {
		game->game_state = 1;
	}

	// strzal z prawej burty
	// todo tworzenie bulletow przez game
	if (key == 'v') {
		Bullet *b = new Bullet(game->player->getPos(), game->player->getDir(), true);
		game->bullets.push_back(b);
	}

	// strzal z lewej burty
	if (key == 'c') {
		Bullet *b = new Bullet(game->player->getPos(), game->player->getDir(), false);
		game->bullets.push_back(b);
	}
}

void OnKeyUp(unsigned char key, int x, int y) {
	keystate[key] = false;
}

// Zapamiêtanie pozycji kursora myszy w momencie, gdy nastêpuje jego przesuniêcie.
// Zapamiêtana pozycja jest póŸniej "konsumowana" przez OnTimer().
void OnMouseMove(int x, int y) {
	mouseX = x;
	mouseY = y;
}

#pragma endregion


//
// Update
//
void OnTimer(int id) {

	glutTimerFunc(17, OnTimer, 0);

	game->update();
	// obsluga wejscia z klawiatury
	keyboardInput();
}

void keyboardInput() {
	// plyniecie do przodu
	if (keystate['w']) {
		game->moveForward();
	}

	// rotacja w lewo
	if (keystate['q'] && keystate['w']) {
		game->rotateLeft();
	}

	// rotacja w prawo
	if (keystate['e'] && keystate['w']) {
		game->rotateRight();
	}
}

void OnRender() {

	T = glutGet(GLUT_ELAPSED_TIME);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.25f, 0.52f, 1.f, 0.f);

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();// and clear

	game->render();

	glutSwapBuffers();
	glFlush();
	glutPostRedisplay();
}

void OnReshape(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, width, height);
	gluPerspective(60.0f, (float) width / height, .01f, 100.0f);
}



