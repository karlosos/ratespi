#include "stdafx.h"
#include "Block.h"


Block::Block()
{
	sphere_radius = 0.8;
	dir.x = 0;
	dir.y = 0;
	dir.z = 0;
	this->rotation = rand() % 50;
}

Block::Block(vec3 pos)
{
	sphere_radius = 0.8;
	this->pos = pos;
	dir.x = 0;
	dir.y = 0;
	dir.z = 0;
}

Block::~Block()
{
}

void Block::render() {
	//drawDetectionSphere();

	float m1_amb[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float m1_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float m1_spe[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	glPushMatrix();
	glTranslatef(pos.x, 0, pos.z);
	glRotatef(0.0f, 1.0f, 0.0f, 1.0f);
	glRotatef(0.0f, 0.0f, rotation, 1.0f);
	double rotate = sin(glutGet(GLUT_ELAPSED_TIME) / 500.f) * 10;
	glRotatef(rotate, 0.0f, 0.0f, 1.0f);
	glutSolidCube(1.0f);
	glPopMatrix();
}

void Block::update() {

}
