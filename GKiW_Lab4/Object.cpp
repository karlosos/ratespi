#include "stdafx.h"
#include "Object.h"


Object::Object()
{
	alive = true;
}


Object::~Object()
{
}

vec3 Object::getPos() {
	return pos;
}

vec3 Object::getDir() {
	return dir;
}

void Object::drawDetectionSphere() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPushMatrix();
	glTranslatef(pos.x, pos.y, pos.z);
	glutSolidSphere(sphere_radius, 5, 5);
	glPopMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}