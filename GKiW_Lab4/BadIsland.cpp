#include "stdafx.h"
#include "BadIsland.h"


BadIsland::BadIsland(vec3 pos) : Island(pos)
{
	cooldown = 0;
}


BadIsland::~BadIsland()
{
}

void BadIsland::render() {
	float m1_amb[] = { 0.9f, 0.2f, 0.2f, 1.0f };
	float m1_dif[] = { 0.9f, 0.2f, 0.2f, 1.0f };
	float m1_spe[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	Island::render();
}

void BadIsland::update() {
	cooldown = cooldown - 0.05;
	printf("%d\n", cooldown);
	Island::update();
}