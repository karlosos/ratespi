// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <GL/freeglut.h>
#include "ObjLoader.h"
#include <vector>
#include "GKiW_Lab4.h"
#include <string>
#include <stdlib.h>
#include <time.h> 
#include <list>