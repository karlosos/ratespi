#include "stdafx.h"
#include "GUI.h"
#include "Game.h"

GUI::GUI()
{
	points = 0;
	boxes = 0;
	hp = 20;
	maxhp = hp;
}


GUI::~GUI()
{
}

// wyswietla ilosc zdobytych punktow
void GUI::renderPoints() {
	glColor3f(0.0, 1.0, 0.0); // Green
	glRasterPos2i(WIN_WIDTH / 2, WIN_HEIGHT - 20);

	std::string s = "Points: ";
	s += std::to_string(points);

	void * font = GLUT_BITMAP_9_BY_15;
	for (std::string::iterator i = s.begin(); i != s.end(); ++i)
	{
		char c = *i;
		glutBitmapCharacter(font, c);
	}
}

// wyswietla ilosc zdobytych pudelek
void GUI::renderBoxes() {
	glColor3f(0.0, 1.0, 0.0); // Green
	glRasterPos2i(WIN_WIDTH / 2, WIN_HEIGHT - 40);

	std::string s = "Boxes: ";
	s += std::to_string(boxes);

	void * font = GLUT_BITMAP_9_BY_15;
	for (std::string::iterator i = s.begin(); i != s.end(); ++i)
	{
		char c = *i;
		glutBitmapCharacter(font, c);
	}
}

// wyswietla ilosc hp
void GUI::renderHP() {
	glColor3f(0.0, 1.0, 0.0); // Green
	glRasterPos2i(WIN_WIDTH / 2, WIN_HEIGHT - 60);

	std::string s = "HP: ";
	s += std::to_string(hp);
	s += "/";
	s += std::to_string(maxhp);

	void * font = GLUT_BITMAP_9_BY_15;
	for (std::string::iterator i = s.begin(); i != s.end(); ++i)
	{
		char c = *i;
		glutBitmapCharacter(font, c);
	}
}

// wyswietla tekst 
void GUI::renderText(std::string s, int offsetFromTop, int offsetLeft) {
	glColor3f(0.0, 1.0, 0.0);
	glRasterPos2i(offsetLeft, WIN_HEIGHT - offsetFromTop - 50);

	void * font = GLUT_BITMAP_9_BY_15;
	for (std::string::iterator i = s.begin(); i != s.end(); ++i)
	{
		char c = *i;
		glutBitmapCharacter(font, c);
	}
}

void GUI::render() {
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0.0, WIN_WIDTH, 0.0, WIN_HEIGHT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	renderPoints();
	renderBoxes();
	renderHP();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void GUI::renderStartScreen() {
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0.0, WIN_WIDTH, 0.0, WIN_HEIGHT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	renderText("Witaj w grze RatesPi", 20);
	renderText("Twoim zadaniem jest zbieranie", 40);
	renderText("porzuconych skrzynek i transportowanie je do zielonych przyjaznych wysp", 60);
	renderText("Uwazaj na czerwone wyspy! To sa twoi wrogowie.", 80);
	renderText("Sterowanie:", 140);
	renderText("w - plyn do przodu", 160);
	renderText("q, e - skrec", 180);
	renderText("c, v - strzelaj", 200);

	renderText("Aby rozpoczac gre wcisnij P!", 240);

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void GUI::renderEndScreen() {
	PlaySound("scream.wav", NULL, SND_ASYNC | SND_FILENAME);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0.0, WIN_WIDTH, 0.0, WIN_HEIGHT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	renderText("Koniec gry :(", 20);

	std::string points_str = "Uzyskana liczba punktow: ";
	points_str += std::to_string(points);

	renderText(points_str, 40);

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}