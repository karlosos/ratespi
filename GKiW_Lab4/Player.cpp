﻿#include "stdafx.h"
#include "Player.h"


Player::Player()
{
	this->objectDisplayList = LoadObj("paper_ship.obj");
	pos.x = 0.0f;
	pos.y = 0.f;
	pos.z = 0.0f;
	dir.x = 0.0f;
	dir.y = 0.0f;
	dir.z = -1.0f;
	speed = .4f;
	rotation = 0;
	velocity = 0;

	// do wykrywania kolizji sphere
	sphere_radius = 1.5;
	is_blocked = 0;
}


Player::~Player()
{
}

void Player::render() {
	//sledzenie kamery
	gluLookAt(
		pos.x - dir.x * 5, pos.y + 12, pos.z - dir.z * 5,
		pos.x, pos.y, pos.z + dir.z,
		0.0f, 1.0f, 0.0f
	);

	//drawDetectionSphere();

	// sledzenie swiatla
	float l0_amb[] = { 0.2f, 0.2f, 0.2f };
	float l0_dif[] = { 1.f, 1.f, 1.f };
	float l0_spe[] = { 0.5f, 0.5f, 0.5f };
	float l0_pos[] = { pos.x , -10.f, pos.z, 1.0f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, l0_amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l0_dif);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l0_spe);
	glLightfv(GL_LIGHT0, GL_POSITION, l0_pos);

	// sledzenie swiatla
	float l1_amb[] = { 0.2f, 0.2f, 0.2f };
	float l1_dif[] = { 0.4f, .4f, .4f };
	float l1_spe[] = { 0.5f, 0.5f, 0.5f };
	float l1_pos[] = { pos.x , 3.f, pos.z, 1.0f };
	glLightfv(GL_LIGHT1, GL_AMBIENT, l1_amb);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, l1_dif);
	glLightfv(GL_LIGHT1, GL_SPECULAR, l1_spe);
	glLightfv(GL_LIGHT1, GL_POSITION, l1_pos);

	// model statku
	float m0_amb[] = { 0.8f, 0.2f, 0.8f, 0.7f };
	float m0_dif[] = { 0.8f, 0.2f, 0.8f, 0.9f };
	float m0_spe[] = { 0.1f, 0.1f, 0.1f, 0.5f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m0_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m0_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m0_spe);
	glPushMatrix();
	glTranslatef(pos.x, 0, pos.z);
	glRotatef(rotation + 180.f, 0.0f, 1.0f, 0.0f);
	double rotate = sin(glutGet(GLUT_ELAPSED_TIME) / 500.f) * 30;
	glRotatef(rotate, 0.0f, 0.0f, 1.0f);
	glScalef(1.f, 1.f, 1.f);

	//drawDetectionBox();

	glCallList(objectDisplayList);
	glPopMatrix();
}

void Player::drawDetectionBox() {
	glPushMatrix();
	glBegin(GL_POLYGON);
	glVertex3f(-0.5, 0, 1.75);
	glVertex3f(-0.5, 0, -1.75);
	glVertex3f(0.5, 0, -1.75);
	glVertex3f(0.5, 0, 1.75);
	glEnd();
	glPopMatrix();
}



void Player::rotateRight() {
	rotation -= 1;
	float beta = atan2(dir.z, dir.x);
	float inclination = acos(dir.y);
	dir.x = sin(inclination)*cos(+1.f * PI / 180.f + beta);
	dir.z = sin(inclination)*sin(+1.f * PI / 180.f + beta);
	dir.y = cos(inclination);
}

void Player::rotateLeft() {
	rotation += 1;
	float beta = atan2(dir.z, dir.x);
	float inclination = acos(dir.y);
	dir.x = sin(inclination)*cos(-1.f * PI / 180.f + beta);
	dir.z = sin(inclination)*sin(-1.f * PI / 180.f + beta);
	dir.y = cos(inclination);
}

void Player::moveForward() {
	velocity = speed;
}

void Player::moveBackward() {
	velocity = -2*speed;
}

void Player::update() {
	// Ruch przod/tyl:
	pos.x += dir.x * velocity * .1f;
	pos.y = 0.f;
	pos.z += dir.z * velocity * .1f;

	// Inercja:
	velocity /= 1.025;
}

vec3 Player::aproxPosition() {
	vec3 v;
	v.x = pos.x + dir.x * velocity + 0.7;
	v.z = pos.z + dir.z * velocity + 0.7;
	v.y = 0;
	return v;
}

vec3 Player::abovePosition() {
	vec3 v = this->pos;
	v.y = 2;
	return v;
}