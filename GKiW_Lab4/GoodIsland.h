#pragma once
#include "Island.h"
class GoodIsland :
	public Island
{
public:
	GoodIsland(vec3 pos);
	~GoodIsland();
	void render();
	void update();
};

