#include "stdafx.h"
#include "Bullet.h"

Bullet::Bullet(vec3 pos, vec3 dir, bool isCreatedFromRightSide)
{
	// z jakiej pozycji i w jakim kierunku ma leciec pocisk
	this->pos = pos;
	// wektor prostopadly
	this->dir.x = -dir.z;
	this->dir.y = 0;
	this->dir.z = dir.x;

	distance = 0;

	// jezeli kierunek true to leci w prawo
	if (isCreatedFromRightSide)
		this->velocity = 1.f;
	// jezeli kierunek false to leci w lewo
	else 
		this->velocity = -1.f;

	// do wykrywania kolizji - sphere
	sphere_radius = 0.5;
}

Bullet::Bullet(vec3 pos, vec3 dir)
{
	// z jakiej pozycji i w jakim kierunku ma leciec pocisk
	this->pos = pos;
	this->dir = dir;
	distance = 0;
	this->velocity = 1.f;
	sphere_radius = 0.5;
}

Bullet::~Bullet()
{
}

void Bullet::render() {
	// drawDetectionSphere();
	// material pocisku
	float m1_amb[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	float m1_dif[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	float m1_spe[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	glPushMatrix();
	glTranslatef(pos.x, pos.y, pos.z);
	glRotatef(0.0f, 1.0f, 0.0f, 1.0f);
	// tworzenie kuli
	glutSolidSphere(0.4f, 10, 10);
	drawDetectionBox();
	glPopMatrix();
}

void Bullet::drawDetectionBox() {
	glPushMatrix();
	float m1_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	float m1_dif[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	float m1_spe[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	glBegin(GL_TRIANGLE_FAN);
	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, -pos.y, 0.0f);
	for (int i = 40; i >= 0; --i) {
		float x = sin(2 * PI * ((float)i / 40)) * 0.5;
		float z = cos(2 * PI * ((float)i / 40)) * 0.5;
		glNormal3f(1.0f, 0.0f, 0.0f);
		glVertex3f(x, -pos.y, z);
	}
	glEnd();

	glPopMatrix();
}

void Bullet::update() {
	distance += 0.1;

	pos.y = -0.1*distance * (distance - 10.f);

	if (pos.y < 0.f) {
		alive = false;
	}

	pos.x += dir.x * velocity * .1f;
	pos.z += dir.z * velocity * .1f;
}