#pragma once
#include "Object.h"
class Enemy :
	public Object
{
public:
	Enemy(vec3 pos);
	~Enemy();
	void render();
	void update();
	// move functions
	void moveForward();
	void moveBackward();
	void randomDirection();
	void rotateRight(int step);
	void rotateLeft(int step);

public:
	float speed;
	float rotation;
	double rotate;
	float velocity;
	GLuint objectDisplayList;
	//void drawDetectionBox();
	bool is_blocked;
};

