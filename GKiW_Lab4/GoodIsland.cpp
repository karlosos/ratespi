#include "stdafx.h"
#include "GoodIsland.h"


GoodIsland::GoodIsland(vec3 pos) : Island(pos)
{

}


GoodIsland::~GoodIsland()
{
}

void GoodIsland::render() {
	float m1_amb[] = { 0.2f, 0.9f, 0.2f, 1.0f };
	float m1_dif[] = { 0.2f, 0.9f, 0.2f, 1.0f };
	float m1_spe[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	Island::render();
}

void GoodIsland::update() {
	Island::update();
}
