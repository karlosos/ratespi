#pragma once
#include "Island.h"
class BadIsland :
	public Island
{
public:
	BadIsland(vec3 pos);
	~BadIsland();
	void render();
	void update();
	double cooldown;
};

