#include "stdafx.h"
#include "Island.h"


Island::Island(vec3 pos)
{
	//std::string file = "island";
	int number_of_model = rand() % 2 + 1;
	if (number_of_model == 1) {
		this->objectDisplayList = LoadObj("island1.obj");
	}
	else {
		this->objectDisplayList = LoadObj("island2.obj");
	}
	sphere_radius = 3;
	this->pos = pos;
	dir.x = 0;
	dir.y = 0;
	dir.z = 0;
}


Island::~Island()
{
}

void Island::render() {
	//drawDetectionSphere();

	glPushMatrix();
	glTranslatef(pos.x, 0, pos.z);
	glRotatef(0.0f, 1.0f, 0.0f, 1.0f);
	//glutSolidCube(1.0f);
	glScalef(3.f, 3.f, 3.f);
	glCallList(objectDisplayList);
	
	glPopMatrix();
}

void Island::update() {

}