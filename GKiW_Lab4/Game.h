#pragma once
#include "GUI.h"
#include "Island.h"
#include "Enemy.h"
#include "GoodIsland.h"
#include "BadIsland.h"

class Game
{
public:
	static Game* getInstance();
	~Game();
	void update();
	void render();
	Player* player;
	std::vector<Bullet*> bullets;
	std::vector<Bullet*> bad_bullets;
	std::vector<GoodIsland*> good_islands;
	std::vector<BadIsland*> bad_islands;
	std::vector<Block*> blocks;

	void moveForward();
	void rotateLeft();
	void rotateRight();

	static void unblockPlayer(int val);
	unsigned int game_state;

private:
	Game();
	static Game* instance;
	int points;
	void addPoints();
	void subPoints();
	bool isCollision(vec3 pos1, double r1, vec3 pos2, double r2);
	double distance(vec3 pos1, vec3 pos2);
	GUI gui;
	vec3 calculateVector(vec3 pos0, vec3 pos1);
};

