#include "stdafx.h"
#include "Game.h"

Game* Game::instance = NULL;

Game* Game::getInstance() {
	if (!instance) {
		instance = new Game();
	}
	return instance;
}

Game::Game()
{
	game_state = 0;
	player = new Player();

	//szescianow
	//for (int i = 0; i < 5; i++) {
	//	for (int j = 0; j < 5; j++) {
	//		vec3 pos;
	//		pos.x = i * 10.0;
	//		pos.y = 0;
	//		pos.z = j * 10.0;
	//		blocks.push_back(new Island(pos));
	//	}
	//}

	// losuj pozycje 50 wysp
	int min_distance = 5;
	int max_board = 100;

	for (int i = 0; i < 150; i++) {
		vec3 rand_pos;
		rand_pos.x = rand() % max_board - max_board/2;
		rand_pos.z = rand() % max_board - max_board/2;
		rand_pos.y = 0;

		bool flag = false;

		if (distance(rand_pos, player->getPos()) < 10)
			flag = true;

		for (auto &island : good_islands) {
			if (distance(rand_pos, island->getPos()) < min_distance) {
				flag = true;
				break;
			}
		}

		if (!flag) {
			for (auto &island : bad_islands) {
				if (distance(rand_pos, island->getPos()) < min_distance) {
					flag = true;
					break;
				}
			}
		}
		if (!flag) {
			if (i % 2) {
				good_islands.push_back(new GoodIsland(rand_pos));
			}
			else {
				bad_islands.push_back(new BadIsland(rand_pos));
			}
		}
	}

	// losuj pozycje blokow
	for (int i = 0; i < 300; i++) {
		vec3 rand_pos;
		rand_pos.x = rand() % max_board - max_board/2;
		rand_pos.z = rand() % max_board - max_board/2;
		rand_pos.y = 0;

		bool flag = false;

		if (distance(rand_pos, player->getPos()) < 10)
			flag = true;

		for (auto &island : good_islands) {
			if (distance(rand_pos, island->getPos()) < min_distance) {
				flag = true;
				break;
			}
		}

		if (!flag) {
			for (auto &island : bad_islands) {
				if (distance(rand_pos, island->getPos()) < min_distance) {
					flag = true;
					break;
				}
			}
		}

		if (!flag) {
			for (auto &block : blocks) {
				if (distance(rand_pos, block->getPos()) < min_distance) {
					flag = true;
					break;
				}
			}
		}
		if (!flag) {
			blocks.push_back(new Block(rand_pos));
		}
	}

	//randomEnemyDirections(1);

	gui.WIN_WIDTH = 1280;
	gui.WIN_HEIGHT = 720;
}


Game::~Game()
{
	// czyscic pamiec
}

void Game::addPoints() {
	points++;
}

void Game::subPoints() {
	points--;
}

void Game::update() {
	if (gui.hp <= 0) {
		game_state = 2;
		PlaySound("scream.wav", NULL, SND_ASYNC | SND_FILENAME);
	}

	if (game_state == 1) {
		for (auto &block : bad_islands) {
			if (block->alive)
				block->update();
		}

		for (auto &bullet : bullets) {
			if (bullet->alive)
				bullet->update();
		}

		for (auto &bullet : bad_bullets) {
			if (bullet->alive)
				bullet->update();
		}

		for (auto &enemy : blocks) {
			if (enemy->alive)
				enemy->update();
		}

		// detekcja kolizji pociskow wysp
		for (auto &bullet : bad_bullets) {
			if (bullet->alive) {
				if (isCollision(bullet->getPos(), bullet->sphere_radius, player->abovePosition(), player->sphere_radius)) {
					printf("Dostales \n");
					gui.hp--;
					bullet->alive = 0;
				}
			}
		}

		// detekcja kolizji pociskow gracza
		for (auto &bullet : bullets) {
			if (bullet->alive) {
				for (auto &block : good_islands) {
					if (block->alive && isCollision(bullet->getPos(), bullet->sphere_radius, block->getPos(), block->sphere_radius)) {
						printf("Kolizja \n");
						gui.points--;
						block->alive = 0;
						bullet->alive = 0;
					}
				}

				for (auto &block : bad_islands) {
					if (block->alive && isCollision(bullet->getPos(), bullet->sphere_radius, block->getPos(), block->sphere_radius)) {
						printf("Kolizja \n");
						gui.points++;
						block->alive = 0;
						bullet->alive = 0;
					}
				}
			}
		}

		// kolizje statku z naszymi wyspami
		for (auto &block : good_islands) {
			if (block->alive) {
				if (isCollision(player->getPos(), player->sphere_radius, block->getPos(), block->sphere_radius)) {
					gui.points += 5 * gui.boxes;
					gui.boxes = 0;
					player->is_blocked = true;
					glutTimerFunc(500, unblockPlayer, 1);
					printf("Cofaj! \n");
					player->moveBackward();
				}
			}
		}

		// kolizje statku z wrogimi wyspami
		// tracimy boxy
		for (auto &block : bad_islands) {
			if (block->alive) {
				// kolizja z naszym statkiem
				if (isCollision(player->getPos(), player->sphere_radius, block->getPos(), block->sphere_radius)) {
					gui.boxes = 0;
					player->is_blocked = true;
					glutTimerFunc(500, unblockPlayer, 1);
					printf("Cofaj! \n");
					player->moveBackward();
				}

				// czy strzelac do gracza
				if (distance(player->getPos(), block->getPos()) < 10.f && block->cooldown <= 1) {
					block->cooldown = 5;
					// oblicz wektor kierunkowy od wyspy do gracza
					vec3 dir = calculateVector(player->aproxPosition(), block->getPos());
					Bullet *b = new Bullet(block->getPos(), dir);
					bad_bullets.push_back(b);
				}
			}
		}

		// zbieranie skrzynek
		for (auto &block : blocks) {
			if (block->alive) {
				if (isCollision(player->getPos(), player->sphere_radius, block->getPos(), block->sphere_radius)) {
					if (gui.boxes < 5) {
						gui.boxes++;
						printf("Zebrales skrzynke! \n");
						block->alive = false;
					}
					else {
						// wyswietl gui masz pelna ilosc skrzynek
					}
				}
			}
		}


		player->update();
	}
}

void Game::render() {
	if (game_state == 0) {
		gui.renderStartScreen();
	}
	else if (game_state == 2) {
		gui.renderEndScreen();
	}
	else {
		gui.render();

		player->render();

		for (auto &block : good_islands) {
			if (block->alive && distance(player->getPos(), block->getPos()) < 30)
				block->render();
		}

		for (auto &block : bad_islands) {
			if (block->alive && distance(player->getPos(), block->getPos()) < 30)
				block->render();
		}

		for (auto &bullet : bullets) {
			if (bullet->alive)
				bullet->render();
		}

		for (auto &bullet : bad_bullets) {
			if (bullet->alive)
				bullet->render();
		}

		for (auto &block : blocks) {
			if (block->alive && distance(player->getPos(), block->getPos()) < 30)
				block->render();
		}
	}
}

void Game::moveForward() {
	if (player->is_blocked == false) {
		player->moveForward();
	}
	
}

void Game::rotateLeft() {
	player->rotateLeft();
}

void Game::rotateRight() {
	player->rotateRight();
}

bool Game::isCollision(vec3 pos0, double r0, vec3 pos1, double r1) {
	if (distance(pos0, pos1) <= r0 + r1) {
		return true;
	}
	return false;
}

double Game::distance(vec3 pos0, vec3 pos1) {
	return sqrt(pow(pos1.x - pos0.x, 2) + pow(pos1.y - pos0.y, 2) + pow(pos1.z - pos0.z, 2));
}

void Game::unblockPlayer(int val) {
	getInstance()->player->is_blocked = false;
}

vec3 Game::calculateVector(vec3 pos0, vec3 pos1) {
	vec3 v;
	v.x = pos0.x - pos1.x;
	v.y = pos0.y - pos1.y;
	v.z = pos0.z - pos1.z;
	
	// oblicz dlugosc wektora
	double length = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	v.x = v.x / length;
	v.y = v.y / length;
	v.z = v.z / length;
	return v;
}